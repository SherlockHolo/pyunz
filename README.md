# pyunz

## usage

```
usage: pyunz.py [-h] [-x EXTRACT] [-v] [-i INPUT] [-t {tgz,tbz,txz,7z,zip}]
                [-o OUTPUT]

an extract tool for tgz, zip, 7z...

optional arguments:
  -h, --help            show this help message and exit
  -x EXTRACT, --extract EXTRACT
                        extract the package automatically
  -v, --version         print version
  -i INPUT, --input INPUT
                        create a package
  -t {tgz,tbz,txz,7z,zip}, --type {tgz,tbz,txz,7z,zip}
                        choose the package type
  -o OUTPUT, --output OUTPUT
                        the package name

```

## support

- tar.gz
- tar.xz
- tar.bz
- zip
- 7z
